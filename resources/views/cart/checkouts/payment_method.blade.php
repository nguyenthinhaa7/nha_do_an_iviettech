@extends('layouts.master')
@section('title', 'Phương thức thanh toán')
@section('content')
    <section class="cart-info">
        <div class="row">
            <div class="col-md-6">
                {{-- phương thức thanh toán --}}
                @include('cart.checkouts.parts.payment_method_info')
            </div>
            <div class="col-md-6">
                {{-- danh sách sản phẩm --}}
                @include('cart.checkouts.parts.product_list')
            </div>
        </div>
    </section>
@endsection
