<section class="payment-method-info">
    <form action="{{ route('cart.checkout.checkout-order') }}" method="POST">
        @csrf
        
        <p>Phương thức vận chuyển</p>
        <div class="d-flex justify-content-between mb-2 border p-2">
            <div>
                <input type="radio" name="shipping_method" value="1" checked id="shipping-method-1">
                <label for="shipping-method-1">Giao hàng tận nơi</label>
                <input type="hidden" name="shipping_fee" value="0">
                @error('shipping_method')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div>
                0đ
            </div>
        </div>

        {{-- Phương thức thanh toán --}}
        <p class="mt-5"><label class="required">Phương thức thanh toán</label></p>
         <div class="mb-3 border p-2">
             <input type="radio" name="payment_method_id" value="1" checked id="payment-method-1">
             <label for="payment-method-1">Thanh toán khi giao hàng (COD)</label>
         </div>
         <div class="mb-3 border p-2">
             <input type="radio" name="payment_method_id" value="2" id="payment-method-2" disabled>
             <label for="payment-method-2">Chuyển khoản qua ngân hàng</label>
         </div>
         @error('payment_method_id')
            <div class="text-danger">{{ $message }}</div>
         @enderror

         <div class="d-flex justify-content-between">
             <a href="{{ route('cart.checkout.shipping-information') }}">Trở lại thông tin vận chuyển</a>
             <button type="submit" class="btn btn-primary">Xác nhận đặt hàng</button>
         </div>

    </form>
</section>
