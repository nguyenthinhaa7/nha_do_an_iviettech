<section class="payment-method-info">

    <form action="{{ route('cart.checkout.handle-shipping-information') }}" method="POST">
        @csrf

        <div class="row">
            <div class="col-md-6">
                <div class="mb-3">
                    <label class="form-label"><label class="required">Họ và tên</label></label>
                    @php
                     $fullname = isset($cartShippingInfo['fullname']) ? $cartShippingInfo['fullname'] : null;
                    @endphp
                    <input type="text" name="fullname" value="{{ old('fullname', $fullname) }}" class="form-control" placeholder="">
                    
                 </div>
            </div>
            <div class="col-md-6">
                <div class="mb-3">
                   <label class="form-label"><label class="required">Số điện thoại</label></label>
                   @php
                     $phone = isset($cartShippingInfo['phone']) ? $cartShippingInfo['phone'] : null;
                    @endphp
                   <input type="number" name="phone" value="{{ old('phone', $phone) }}" class="form-control" placeholder="">
                   
                </div>
            </div>
        </div>

         <div class="mb-3">
            <label class="form-label required">Email</label>
            @php
            $email = isset($cartShippingInfo['email']) ? $cartShippingInfo['email'] : null;
           @endphp
            <input type="email" name="email" value="{{ old('email', $email) }}" class="form-control" placeholder="">
            
         </div>

         <div class="mb-3">
            <label class="form-label"><label class="required">Địa chỉ giao hàng</label></label>
            @php
                $address = isset($cartShippingInfo['address']) ? $cartShippingInfo['address'] : null;
            @endphp
            <input type="text" name="address" value="{{ old('address', $address) }}" class="form-control" placeholder="">
           
         </div>

         <div class="mb-3">
            <label class="form-label">Để lại lời nhắn</label>
            @php
                $comment = isset($cartShippingInfo['comment']) ? $cartShippingInfo['comment'] : null;
            @endphp
            <textarea name="comment" class="form-control" rows="2">{{ old('comment', $comment) }}</textarea>
           
         </div>

         <div class="d-flex justify-content-between">
             <a href="{{ route('cart.index') }}">Giỏ hàng</a>
             <button type="submit" class="btn btn-primary">Xác nhận thông tin</button>
         </div>
    </form>
</section>
