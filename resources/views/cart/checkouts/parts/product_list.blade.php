@if (!empty($carts))
    <div class="">
        @foreach($carts as $cart)
            @php $productInfo = $cart['product_info']; @endphp
            <div class="">
                <div class="">
                    <div class="" >
                        <div class="w-24">
                            <span class="">{{ $cart['cart_quantity'] }}</span>
                            <img src="{{ $productInfo->thumbnail }}" alt="{{ $productInfo->name }}" class="">
                        </div>
                        <div class="">
                            <p class="px-2">{{ $productInfo->name }}</p>
                        </div>
                    </div>
                    <div >
                        @if($cart['amount_discount'])
                            <div class="">
                                <span class="">
                                    {!! ($cart['amount_discount']) !!}</span>&nbsp;&nbsp;&nbsp;
                                <span class="">
                                    {!! ($cart['amount']) !!}</span>
                            </div>
                        @else
                            <div class="">
                                <span class="">
                                    {!! ($cart['amount']) !!}</span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
            
        <hr>

        <div class="cart-info">
            <div class="d-flex justify-content-between">
                <label>Tạm tính</label>
                @if($total_amount_discount != $total_amount)
                    <span>
                        <span class="sale-price">{{ ($total_amount_discount) }}</span>
                        <span class="sale-price-through">{{ ($total_amount) }}</span>
                    </span>
                @else
                    <span>
                        <span class="sale-price">{{ ($total_amount_discount) }}</span>
                    </span>
                @endif
            </div>
            <div class="d-flex justify-content-between">
                <label>Phí shíp</label>
                <span>Miễn phí</span>
            </div>
        </div>

        <hr>

        <div class="d-flex justify-content-between">
            <label class="fw-bold fs-1">Tổng tiền</label>
            @if($total_amount_discount != $total_amount)
                <span>
                    <span class="fw-bold fs-1 sale-price">{{ ($total_amount_discount) }}</span>
                    <span class="sale-price-through">{{ ($total_amount) }}</span>
                </span>
            @else
                <span>
                    <span class="fw-bold fs-1 sale-price">{{ ($total_amount) }}</span>
                </span>
            @endif
        </div>
    </div>
@endif
