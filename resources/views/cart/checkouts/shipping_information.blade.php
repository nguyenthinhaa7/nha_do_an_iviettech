@extends('layouts.master')
@section('title', 'Thông tin vận chuyển')
@section('content')
    <section class=" bg-blue-50 mx-24 my-3">
        <h1>THÔNG TIN THANH TOÁN</h1>
        <div class="flex">
            <div class="">
                {{-- thông tin thanh toán --}}
                @include('cart.checkouts.parts.shipping_information_info')
            </div>
            <div class="">
                {{-- danh sách sản phẩm --}}
                @include('cart.checkouts.parts.product_list')
            </div>
        </div>
    </section>
@endsection    

