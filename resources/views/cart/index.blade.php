@extends('layouts.master')
@section('title', 'Danh sách sản phẩm')
@section('content')
<nav aria-label="" class="my-3 ">
    <ol class="">  
        <li class="text-3xl text-center font-bold" >Giỏ hàng</li>
    </ol>
</nav>
@if(!empty($carts))
    <div class="mx-56  border-2 border-indigo-500">
        <table class="w-full ">
            <thead class="">
                <tr class="border-b border-indigo-500 ">
                    <th class="border-r border-indigo-500">#</th>
                    <th class="border-r border-indigo-500"> Tên</th>
                    <th class="border-r border-indigo-500"> Hình ảnh</th>
                    <th class="border-r border-indigo-500"> Giá</th>
                    <th class="border-r border-indigo-500">Số lượng</th>
                    <th class="border-r border-indigo-500">Tổng tiền</th>
                    <th class="" colspan="2">Thành phần</th>
                </tr>
            </thead>

            <tbody class="text-center">
                @php
                    $totalQuantity = 0;
                    $totalMoney = 0;
                @endphp

                @foreach($carts as $key => $value)
                    @php
                        $productInfo = $value['cart_product_info'];
                        $totalQuantity += $value['cart_quantity'];
                        $money = $productInfo->price * $value['cart_quantity'];
                        $totalMoney += $money;    
                    @endphp

                    <tr class="border-b border-indigo-500">
                        <td class="border-r border-indigo-500">{{ ++$key }}</td>
                        <td class="border-r border-indigo-500">{{ $productInfo->name }}</td>
                        <td class="border-r border-indigo-500">
                            <img src="{{ $productInfo->thumbnail }}" alt="{{ $productInfo->name }}" class="ml-9" width="120">
                        </td>
                        <td class="border-r border-indigo-500 ">{{ number_format($productInfo->price) }} VNĐ</td>
                        <td class="border-r border-indigo-500">{{ number_format($value['cart_quantity']) }}</td>
                        <td class="border-r border-indigo-500">
                            {{ number_format($money) }} VNĐ
                        </td>
                        <td class="border-r border-indigo-500 w-36">
                            <form action="{{route('cart.update-quantity' , $productInfo->id)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class=" border border-indigo-500 w-8/9">
                                    <input class="w-8/9"type="number" name="quantity" value="{{ $value['cart_quantity'] }}">
                                </div>
                                <button type="submit" class="border-2 border-blue-500  bg-blue-600 mt-5 text-white w-24">Cập nhật</button>
                            </form>
                        </td>
                        <td class="">
                            <form action="{{ route('cart.remove-product-in-cart', $productInfo->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class=" border-2 border-red-500 bg-red-500 text-white w-12">Xóa</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot class="bg-info text-center text-red-600 text-xl">
                <tr class="">
                    <td colspan="4" class=" text-xl">Tổng cộng</td>
                    <td class="fw-bold">{{ number_format($totalQuantity) }}</td>
                    <td class="text-red-600">{{ number_format($totalMoney) }} VNĐ</td>
                    <td colspan="2"></td>
                </tr>
            </tfoot>
        </table>      
    </div>
    <div class="mt-5 mb-5 text-center">
        <a href="{{ route('home.index') }}" class="border-2 border-blue-500 bg-blue-600 text-white ">Tiếp tục mua hàng</a>
    </div>
@else
    <p class="text-center text-4xl text-red-600 mt-36">Giỏ hàng trống.</p>
    <div class="mt-5 mb-5 text-center">
        <a href="{{ route('home.index') }}" class="border-2 border-blue-500 bg-blue-600 text-white">Tiếp tục mua hàng</a>
    </div>
@endif
<!-- <nav aria-label="" class="my-3 ">
    <ol class="">  
        <li class="text-3xl text-center font-bold" >Thông tin thanh toán</li>
    </ol>
</nav>
<div class="mx-56  border-2 border-indigo-500 flex">
    <div class="w-2/3 ">
        <form action="" method="POST">
            @csrf     
            <div class="mb-3 ml-12 flex mt-3">
                <label class="form-label required text-xl w-1/3">Họ và Tên</label>
                <input type="text"  class="border border-blue-300  w-96 mr-24" placeholder="">
            </div>
            <div class="mb-3 ml-12 flex">
                <label class="form-label required text-xl w-1/3">Số điện thoại</label>
                <input type="text"  class="border border-blue-300  w-96 mr-24" placeholder="">
            </div>

            <div class="mb-3 ml-12 flex">
                <label class="form-label required text-xl w-1/3">Email</label>
                <input type="text"  class="border border-blue-300  w-96 mr-24" placeholder="">
            </div>
            <div class="mb-3 ml-12 flex">
                <label class="form-label required text-xl w-1/3">Địa chỉ nhận</label>
                <input type="text"  class="border border-blue-300  w-96 mr-24" placeholder="">
            </div>
            <div class="mb-3 ml-12 flex">
                <label class="form-label required text-xl w-1/3">Lời nhắn</label>
                <input type="text"  class="border border-blue-300  w-96 mr-24" placeholder="">
            </div>
        </form>
    </div> 
    <div class="h-12 my-24 mx-12 border border-blue-300 bg-red-600 rounded-md">
       <div class="">
            <a href="" class="  text-4xl ">HOÀN TẤT</a>
            
        </div>
    </div>     
</div> -->
<div class="mt-5 mb-5 text-center text-3xl">
    <button type="button" 
    class="border-2 border-blue-500 bg-blue-600 text-white" id="btn-checkout"
        onclick="window.location.href='{{ route('cart.checkout.shipping-information') }}'">
        Thanh toán
    </button>
</div>

@endsection

