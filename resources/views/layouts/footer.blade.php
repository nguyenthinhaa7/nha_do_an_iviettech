<footer >
    <div class="">
        <nav class="flex mx-24 my-1 bg-blue-50">
            <ul class="ml-32 w-1/3 my-5">
                <li>Tích điểm quà tặng VIP</li>
                <li>Lịch sử mua hàng</li>
                <li>Tìm hiểu về mua trả góp</li>
                <li>Chính sách bảo hanh</li>
            </ul>
            <ul class="ml-32 w-1/3 my-5">
                <li>Giới thiệu công ty</li>
                <li>Tuyển dụng</li>
                <li>Gửi góp ý, khiếu nại</li>
                <li>Tìm siêu thị</li>
            </ul>
            <ul class=" w-1/3 my-5">
                <li class=" font-bold">Tổng đài hỗ trợ</li>
                <li>Gọi mua: 18009999 (7:30 - 22:00)</li>
                <li>Kỹ thuật: 18008888 (7:00 - 23:00)</li>
                <li>Bảo hành: 18007777 (8:00 - 20:00)</li>
            </ul>
        </nav>
    </div>
</footer>