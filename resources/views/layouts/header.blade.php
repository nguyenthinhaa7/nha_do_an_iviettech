<header>
    <div class=" ">
        <nav class="flex justify-between items-center  bg-yellow-400">
            <div class="flex ml-10">
                <img class="w-24 " src="/logo/logo.png" alt="">
                <h1 class="text-6xl m-5 ml-28 font-mono">NHA MOBILE</h1>
            </div>
            <ul class="flex justify-between items-center py-5">
                <div class="flex">
                    <img class="w-12 " src="/icon/home1.png" alt="">
                    <li class=" mt-3 text-xl"><a href="{{ route('home.index') }}">Trang chủ</a></li>
                </div>
                <div class="flex ml-12">
                    <img class="w-5 h-5 mt-3 " src="/icon/history.png" alt="">
                    <li class=" mt-2 ml-3 text-xl"><a href="{{ route('home.index') }}">Lịch sử</a></li>
                </div><div class="flex ml-12">
                    <img class="w-10 h-5 mt-2 " src="/icon/shopping1.png" alt="">
                    <li class=" mt-1 text-xl"><a href="cart">Giỏ hàng</a></li>
                </div><div class="flex ml-12 mr-16">
                @include('layouts.login')
                </div>
                
                
            </ul>
        </nav>
        
        <div class="flex">
            <img class="w-1/3 " src="https://images.fpt.shop/unsafe/fit-in/800x300/filters:quality(90):fill(white)/fptshop.com.vn/Uploads/Originals/2022/10/24/638021695897481292_F-H1_800x300.png" alt="">
            <img class="w-1/3 " src="https://images.fpt.shop/unsafe/fit-in/800x300/filters:quality(90):fill(white)/fptshop.com.vn/Uploads/Originals/2022/10/4/638004801120320356_F-H1_800x300.png" alt="">
            <img class="w-1/3" src="https://images.fpt.shop/unsafe/fit-in/800x300/filters:quality(90):fill(white)/fptshop.com.vn/Uploads/Originals/2022/10/15/638014290717253928_F-H1_800x300.png" alt="">
        </div>
        
        
        
            
        <div class="mx-24  bg-black h-24 mt-3">
            
        <div class=" bg-black text-xl w-96 h-12 ml-7">
            <form action="{{ route('product.index') }}" method="GET">
                <div class=" mb-3 flex">
                    <div >
                        <input type="text" name="keyword" value="{{ request()->get('keyword') }}" class=" m-3" placeholder="Tìm kiếm ">
                    </div>
                    <div class=" text-xl">
                    <button type="submit" class="  bg-blue-600 m-3 text-white" ><span class=""></span><span class="m-3">Tìm kiếm</span></button>
                    </div>        
                </div>
            </form>
        </div>
            <ul class="flex text-white ">
                <li class="ml-10 mr-5 mt-3"><a href="{{ route('home.index') }}">ĐIỆN THOẠI</a></li>
                <li class="mx-5 mt-3"><a href="#">LAPTOP</a></li>
                <li class="mx-5 mt-3"><a href="#">MÁY TÍNH BẢNG</a></li>
                <li class="mx-5 mt-3"><a href="#">APPLE</a></li>
                <li class="mx-5 mt-3"><a href="#">PC - LINH KIỆN</a></li>
                <li class="mx-5 mt-3"><a href="#">PHỤ KIỆN</a></li>
                <li class="mx-5 mt-3"><a href="#">MÁY CỦ GIÁ RẺ</a></li>
                <li class="mx-5 mt-3"><a href="#">HÀNG GIA DỤNG</a></li>
                <li class="mx-5 mt-3"><a href="#">SIM - THẺ</a></li>
                <li class="mx-5 mt-3"><a href="#">KHUYẾN MÃI</a></li>
                
            </ul>
            
        </div>
    </div>
</header>