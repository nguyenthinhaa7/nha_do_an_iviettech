@auth
    <!-- <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle shadow-none" type="button" 
        id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            <span class="fa fa-user"></span>&nbsp;
            <span class="text">Xin chào,&nbsp;</span><span>{{ Auth::user()->name }}</span>
        </button>
        <ul class="dropdown-menu account-info" aria-labelledby="dropdownMenuButton1">
            <li><a class="dropdown-item active" href="{{ route('my-profile') }}"><i class="fa fa-info" aria-hidden="true"></i>&nbsp;
                Thông tin tài khoản</a></li>
            <li>
                <hr class="dropdown-divider">
            </li>
            <li><a class="dropdown-item" href=""><i class="fa fa-history" aria-hidden="true"></i>&nbsp;
                Lịch sử mua hàng</a></li>
            <li>
                <hr class="dropdown-divider">
            </li>
            <li><a class="dropdown-item" href=""><i class="fa fa-key" aria-hidden="true"></i>&nbsp;
                Đổi mật khẩu</a></li>
            <li>
                <hr class="dropdown-divider">
            </li>
            <li>
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn"><i class="fas fa-sign-out-alt"></i>&nbsp;
                        <span class="text">Logout</span></button>
                </form>
            </li>
        </ul>
    </div> -->
    <div class="flex justify-center">
  <div>
    <div class="dropdown relative">
      <button
        class="dropdown-toggle px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded 
        shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none 
        focus:ring-0 active:bg-blue-800 active:shadow-lg active:text-white transition duration-150 ease-in-out 
        flex items-center whitespace-nowrap" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><span class="text">Xin chào,&nbsp;</span><span>{{ Auth::user()->name }}</span>
        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="caret-down" class="w-2 ml-2"role="img"xmlns="http://www.w3.org/2000/svg"viewBox="0 0 320 512">
            <path fill="currentColor" d="M31.3 192h257.3c17.8 0 26.7 21.5 14.1 34.1L174.1 354.8c-7.8 7.8-20.5 7.8-28.3 0L17.2 226.1C4.6 213.5 13.5 192 31.3 192z"></path>
        </svg>
      </button>
      <ul class="dropdown-menu min-w-max absolute bg-white text-base z-50 float-left py-2 list-none text-left rounded-lg shadow-lg mt-1 hidden m-0 bg-clip-padding border-none" aria-labelledby="dropdownMenuButton1">
        <li>
          <a class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-700 hover:bg-gray-100 "href="#" >&nbsp; Thông tin tài khoản</a>
        </li>
        <li>
          <a class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-700 hover:bg-gray-100 "href="#" >&nbsp; Đổi mật khẩu</a>
        </li>
        <li>
          <a class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-700 hover:bg-gray-100 "href="#" >
          <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn"><i class="fas fa-sign-out-alt"></i>&nbsp;
                        <span class="text">Logout</span></button>
                </form>
          </a>
        </li>
        
      </ul>
    </div>
  </div>
</div>
<li>
          <a class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-700 hover:bg-gray-100 "href="#" >
          <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn"><i class="fas fa-sign-out-alt"></i>&nbsp;
                        <span class="text">Logout</span></button>
                </form>
          </a>
        </li>
@endauth
    
@guest

<div class="flex justify-center">
  <div>
    <div class="dropdown relative">
      <button
        class="dropdown-toggle px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded 
        shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none 
        focus:ring-0 active:bg-blue-800 active:shadow-lg active:text-white transition duration-150 ease-in-out 
        flex items-center whitespace-nowrap" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">&nbsp;Tài khoản
        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="caret-down" class="w-2 ml-2"role="img"xmlns="http://www.w3.org/2000/svg"viewBox="0 0 320 512">
            <path fill="currentColor" d="M31.3 192h257.3c17.8 0 26.7 21.5 14.1 34.1L174.1 354.8c-7.8 7.8-20.5 7.8-28.3 0L17.2 226.1C4.6 213.5 13.5 192 31.3 192z"></path>
        </svg>
      </button>
      <ul class="dropdown-menu min-w-max absolute bg-white text-base z-50 float-left py-2 list-none text-left rounded-lg shadow-lg mt-1 hidden m-0 bg-clip-padding border-none" aria-labelledby="dropdownMenuButton1">
        <li>
          <a class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-700 hover:bg-gray-100 "href="{{ route('login') }}" >&nbsp; Đăng nhập</a>
        </li>
        <li>
          <a class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-700 hover:bg-gray-100 "href="{{ route('register') }}" >&nbsp; Đăng ký</a>
        </li>
       
        
      </ul>
    </div>
  </div>
</div>
<li>
          <a class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-700 hover:bg-gray-100 "href="{{ route('login') }}" >&nbsp; Đăng nhập</a>
        </li>

    <!-- <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle shadow-none" type="button" 
        id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
        <span class="fa fa-user"></span><span class="text">&nbsp;Tài khoản</span>
        </button>
        <ul class="dropdown-menu dropdown-menu-dark account-info" aria-labelledby="dropdownMenuButton2">
            <li><a class="dropdown-item" href="{{ route('login') }}"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;
                Đăng nhập</a></li>
            <li>
                <hr class="dropdown-divider">
            </li>
            <li><a class="dropdown-item" href="{{ route('register') }}"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;
                Tạo tài khoản</a></li>
        </ul>
    </div> -->
    
@endguest


<!-- <div class="flex justify-center">
  <div>
    <div class="dropdown relative">
      <button
        class="dropdown-toggle px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded 
        shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none 
        focus:ring-0 active:bg-blue-800 active:shadow-lg active:text-white transition duration-150 ease-in-out 
        flex items-center whitespace-nowrap" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">Dropdown button
        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="caret-down" class="w-2 ml-2"role="img"xmlns="http://www.w3.org/2000/svg"viewBox="0 0 320 512">
            <path fill="currentColor" d="M31.3 192h257.3c17.8 0 26.7 21.5 14.1 34.1L174.1 354.8c-7.8 7.8-20.5 7.8-28.3 0L17.2 226.1C4.6 213.5 13.5 192 31.3 192z"></path>
        </svg>
      </button>
      <ul class="dropdown-menu min-w-max absolute bg-white text-base z-50 float-left py-2 list-none text-left rounded-lg shadow-lg mt-1 hidden m-0 bg-clip-padding border-none" aria-labelledby="dropdownMenuButton1">
        <li>
          <a class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-700 hover:bg-gray-100 "href="#" >Action</a>
        </li>
        <li>
          <a class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-700 hover:bg-gray-100 "href="#" >Action</a>
        </li>
        <li>
          <a class="dropdown-item text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-gray-700 hover:bg-gray-100 "href="#" >Action</a>
        </li>
        
      </ul>
    </div>
  </div>
</div> -->