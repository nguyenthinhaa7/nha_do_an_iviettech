@extends('layouts.master')
@section('title', 'Home')
@section('content')
<div class="bg-blue-50 mx-24 my-1">
    <div class="flex ">
        @foreach ($products as $product)
        <div class="flex  my-1 h-max bg-blue-50">
            <div class="m-5  bg-white ">
                <img class="w-64 m-5" src=" {{$product->thumbnail}} " alt="">
                <h1 class="text-center text-xl"> <a href="">{{$product->name}}</a></h1>
                <h2 class="text-center text-2xl mb-5 font-bold text-red-600"> <a href="{{ route('product.detail', $product['id']) }}">{{number_format($product->price)}} VNĐ</a> </h2>
            </div>
        </div>
        @endforeach
    </div> 
    <div class="mx-5">
        {{ $products->appends(request()->input())->links() }}
    </div>
</div>
@endsection



