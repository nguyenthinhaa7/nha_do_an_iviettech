<?php

namespace Database\Seeders;

use App\Models\ProductImage;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        include 'data_fake/iphone/ip11.php';
        $productimages = [
            ['product_id' => '1' , 'name' => 'image1', 'image' => '/iphone/ip11/iphone11_1.jpg'],
            ['product_id' => '1' , 'name' => 'image2', 'image' => '/iphone/ip11/iphone11_2.jpg' ],
            ['product_id' => '1' , 'name' => 'image3', 'image' => '/iphone/ip11/iphone11_3.jpg' ],
            ['product_id' => '1' , 'name' => 'image4', 'image' => '/iphone/ip11/iphone11_4.jpg' ],

            ['product_id' => '2' , 'name' => 'image1', 'image' => '/iphone/ip13promax/iphone13promax_1.jpg' ],
            ['product_id' => '2' , 'name' => 'image2', 'image' => '/iphone/ip13promax/iphone13promax_2.jpg' ],
            ['product_id' => '2' , 'name' => 'image3', 'image' => '/iphone/ip13promax/iphone13promax_3.jpg' ],
            ['product_id' => '2' , 'name' => 'image4', 'image' => '/iphone/ip13promax/iphone13promax_4.jpg' ],

            ['product_id' => '3' , 'name' => 'image1', 'image' => '/iphone/ip14promax/iphone14promax_1.jpg' ],
            ['product_id' => '3' , 'name' => 'image2', 'image' => '/iphone/ip14promax/iphone14promax_2.jpg' ],
            ['product_id' => '3' , 'name' => 'image3', 'image' => '/iphone/ip14promax/iphone14promax_3.jpg' ],
            ['product_id' => '3' , 'name' => 'image4', 'image' => '/iphone/ip14promax/iphone14promax_4.jpg' ],

            ['product_id' => '4' , 'name' => 'image1', 'image' => '/iphone/ip14pro/iphone14pro_1.jpg' ],
            ['product_id' => '4' , 'name' => 'image2', 'image' => '/iphone/ip14pro/iphone14pro_2.jpg' ],
            ['product_id' => '4' , 'name' => 'image3', 'image' => '/iphone/ip14pro/iphone14pro_3.jpg' ],
            ['product_id' => '4' , 'name' => 'image4', 'image' => '/iphone/ip14pro/iphone14pro_4.jpg' ],

            ['product_id' => '5' , 'name' => 'image1', 'image' => '/iphone/ip14plus/iphone14plus_1.jpg' ],
            ['product_id' => '5' , 'name' => 'image2', 'image' => '/iphone/ip14plus/iphone14plus_2.jpg' ],
            ['product_id' => '5' , 'name' => 'image3', 'image' => '/iphone/ip14plus/iphone14plus_3.jpg' ],
            ['product_id' => '5' , 'name' => 'image4', 'image' => '/iphone/ip14plus/iphone14plus_4.jpg' ],

            ['product_id' => '6' , 'name' => 'image1', 'image' => '/iphone/ip13pro/iphone13pro_1.jpg' ],
            ['product_id' => '6' , 'name' => 'image2', 'image' => '/iphone/ip13pro/iphone13pro_2.jpg' ],
            ['product_id' => '6' , 'name' => 'image3', 'image' => '/iphone/ip13pro/iphone13pro_3.jpg' ],
            ['product_id' => '6' , 'name' => 'image4', 'image' => '/iphone/ip13pro/iphone13pro_4.jpg' ],

            ['product_id' => '7' , 'name' => 'image1', 'image' => '/iphone/ip14/iphone14_1.jpg'],
            ['product_id' => '7' , 'name' => 'image2', 'image' => '/iphone/ip14/iphone14_2.jpg' ],
            ['product_id' => '7' , 'name' => 'image3', 'image' => '/iphone/ip14/iphone14_3.jpg' ],
            ['product_id' => '7' , 'name' => 'image4', 'image' => '/iphone/ip14/iphone14_4.jpg' ],

            ['product_id' => '8' , 'name' => 'image1', 'image' => '/iphone/ip13/iphone13_1.jpg'],
            ['product_id' => '8' , 'name' => 'image2', 'image' => '/iphone/ip13/iphone13_2.jpg' ],
            ['product_id' => '8' , 'name' => 'image3', 'image' => '/iphone/ip13/iphone13_3.jpg' ],
            ['product_id' => '8' , 'name' => 'image4', 'image' => '/iphone/ip13/iphone13_4.jpg' ],

            ['product_id' => '9' , 'name' => 'image1', 'image' => '/iphone/ip13mini/iphone13mini_1.jpg'],
            ['product_id' => '9' , 'name' => 'image2', 'image' => '/iphone/ip13mini/iphone13mini_2.jpg' ],
            ['product_id' => '9' , 'name' => 'image3', 'image' => '/iphone/ip13mini/iphone13mini_3.jpg' ],
            ['product_id' => '9' , 'name' => 'image4', 'image' => '/iphone/ip13mini/iphone13mini_4.jpg' ],

            ['product_id' => '10' , 'name' => 'image1', 'image' => '/iphone/ip12/iphone12_1.jpg'],
            ['product_id' => '10' , 'name' => 'image2', 'image' => '/iphone/ip12/iphone12_2.jpg' ],
            ['product_id' => '10' , 'name' => 'image3', 'image' => '/iphone/ip12/iphone12_3.jpg' ],
            ['product_id' => '10' , 'name' => 'image4', 'image' => '/iphone/ip12/iphone12_4.jpg' ],
        ];
      foreach ($productimages as $productimage){
          ProductImage::factory()
              ->create($productimage);
      }   
    }
}
