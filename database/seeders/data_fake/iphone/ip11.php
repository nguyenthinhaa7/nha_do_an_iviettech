<?php
$iphone11 = 
[    
    'products' => 
    [
        'name' => 'Iphone 11',
        'thumbnail' => '/iphone/ip11/iphone11_0.jpg',
        'price' => '11000000.00',
        'status' =>'1',
        'quantity'=>'10',
        'content' => 'Apple đã chính thức trình làng bộ 3 siêu phẩm iPhone 11, trong đó phiên bản iPhone 11 64GB có mức giá rẻ nhất nhưng vẫn được nâng cấp mạnh mẽ như iPhone Xr ra mắt trước đó.',
       
    ],

    [    
        'name' => 'Iphone 13 Pro Max',
        'thumbnail' => '/iphone/ip13promax/iphone13promax_0.jpg',
        'price' => '28000000.00',
        'status' =>'1',
        'quantity'=>'10',
        'content' => 'Điện thoại iPhone 13 Pro Max 128 GB - siêu phẩm được mong chờ nhất ở nửa cuối năm 2021 đến từ Apple.',
    ],
    [    
        'name' => 'Iphone 14 Pro Max',
        'thumbnail' => '/iphone/ip14promax/iphone14promax_0.jpg',
        'price' => '33900000.00',
        'status' =>'1',
        'quantity'=>'10',
        'content' => 'Cuối cùng thì chiếc iPhone 14 Pro Max cũng đã chính thức lộ diện tại sự kiện ra mắt thường niên vào ngày 08/09 đến từ nhà Apple.',
    ],
    [    
        'name' => 'Iphone 14 Pro',
        'thumbnail' => '/iphone/ip14pro/iphone14pro_0.jpg',
        'price' => '30900000.00',
        'status' =>'1',
        'quantity'=>'10',
        'content' => 'Tại sự kiện ra mắt sản phẩm thường niên diễn ra vào tháng 9/2022, Apple đã trình làng iPhone 14 Pro.',

    ],
    [    
        'name' => 'Iphone 14 Plus',
        'thumbnail' => '/iphone/ip14plus/iphone14plus_0.jpg',
        'price' => '26900000.00',
        'status' =>'1',
        'quantity'=>'10',
        'content' => 'iPhone 14 Plus thu hút mọi ánh nhìn trong sự kiện Far Out diễn ra ngày 8/9 nhờ có vẻ ngoài cao cấp, trang bị bộ xử lý mạnh mẽ.',
        
    ],
    [    
        'name' => 'Iphone 13 Pro',
        'thumbnail' => '/iphone/ip13pro/iphone13pro_0.jpg',
        'price' => '25900000.00',
        'status' =>'1',
        'quantity'=>'10',
        'content' => 'Mỗi lần ra mắt phiên bản mới là mỗi lần iPhone chiếm sóng trên khắp các mặt trận và lần này cái tên khiến vô số người "sục sôi" là iPhone 13 Pro.',
       
    ],
    [
        'name' => 'Iphone 14',
        'thumbnail' => '/iphone/ip14/iphone14_0.jpg',
        'price' => '23000000.00',
        'status' =>'1',
        'quantity'=>'10',
        'content' => 'Sau bao khoảng thời gian dài chờ đợi thì vào ngày 08/09 chiếc điện thoại iPhone 14 cũng đã chính thức được lộ diện.',
       
    ],
    [
        'name' => 'Iphone 13',
        'thumbnail' => '/iphone/ip13/iphone13_0.jpg',
        'price' => '19700000.00',
        'status' =>'1',
        'quantity'=>'10',
        'content' => 'Trong khi sức hút đến từ bộ 4 phiên bản iPhone 12 vẫn chưa nguội đi, thì hãng điện thoại Apple đã mang đến cho người dùng một siêu phẩm mới iPhone 13.',
        
    ],
    [
        'name' => 'Iphone 13 mini',
        'thumbnail' => '/iphone/ip13mini/iphone13mini_0.jpg',
        'price' => '17200000.00',
        'status' =>'1',
        'quantity'=>'10',
        'content' => 'iPhone 13 mini được Apple ra mắt với hàng loạt nâng cấp về cấu hình và các tính năng hữu ích, lại có thiết kế vừa vặn. ',
       
    ],
    [
        'name' => 'Iphone 12',
        'thumbnail' => '/iphone/ip12/iphone12_0.jpg',
        'price' => '16700000.00',
        'status' =>'1',
        'quantity'=>'10',
        'content' => 'Trong những tháng cuối năm 2020, Apple đã chính thức giới thiệu đến người dùng cũng như iFan thế hệ iPhone 12 series mới.',
        
    ],
];
