<?php

namespace Database\Seeders;

use App\Models\Order;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $orders = [
            ['user_id' => '1' , 'name' => 'Nguyễn Thi Nhã' , 'email' => 'nguyenthinhaa7@gmail.com', 'phone' => '0768270720' , 'address' => 'Quảng Trị' , 'status' => '1' ,'payment_method' => 'atm'],
            ['user_id' => '2' , 'name' => 'Lê Văn Nam' , 'email' => 'levannam@gmail.com', 'phone' => '0758320152' , 'address' => 'Huế' , 'status' => '1' ,'payment_method' => 'cash'],
            ['user_id' => '3' , 'name' => 'Bùi Cường' , 'email' => 'buicuong@gmail.com', 'phone' => '0956231502' , 'address' => 'Đà Nẵng' , 'status' => '1' ,'payment_method' => 'atm'],
            ['user_id' => '4' , 'name' => 'Trần Quốc Khánh' , 'email' => 'tranquockhanh@gmail.com', 'phone' => '0856212320' , 'address' => 'Quảng Nam' , 'status' => '1' ,'payment_method' => 'cash'],
            ['user_id' => '5' , 'name' => 'Văn Đình Quốc' , 'email' => 'vandinhquoc@gmail.com', 'phone' => '0769999999' , 'address' => 'Quảng Bình' , 'status' => '1' ,'payment_method' => 'cash'],

        ];
      foreach ($orders as $order){
          Order::factory()
              ->create($order);
      }
    }
}
