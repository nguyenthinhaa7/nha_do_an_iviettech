<?php

namespace Database\Seeders;

use App\Models\Comment;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $comments = [
            ['user_id' => '1' , 'product_id' => '1' , 'content' => 'good' , 'rate' => '4'],
            ['user_id' => '2' , 'product_id' => '2' , 'content' => 'very good' , 'rate' => '5'],
            ['user_id' => '3' , 'product_id' => '3' , 'content' => 'beautiful' , 'rate' => '3'],
            ['user_id' => '4' , 'product_id' => '1' , 'content' => 'great' , 'rate' => '2'],
            ['user_id' => '5' , 'product_id' => '3' , 'content' => 'excellent ', 'rate' => '1'],

        ];
      foreach ($comments as $comment){
          Comment::factory()
              ->create($comment);
      }   
    }
}
