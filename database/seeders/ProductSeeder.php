<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        include 'data_fake/iphone/ip11.php';
        foreach ($iphone11 as $ip11) 
        {
            Product::create
            ([
                'name' => $ip11['name'],
                'thumbnail' => $ip11['thumbnail'],
                'content' => $ip11['content'],
                'status' => $ip11['status'],
                'quantity' => $ip11['quantity'],
                'price' => $ip11['price'], 
                 
            ]);
        }
    }
}
