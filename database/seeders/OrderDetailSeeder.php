<?php

namespace Database\Seeders;

use App\Models\OrderDetail;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $orderdetails = [
            ['product_id' => '1' , 'order_id' => '1' , 'quantity' => '1', 'price' => '11000000'],
            ['product_id' => '2' , 'order_id' => '2' , 'quantity' => '2', 'price' => '56000000'],
            ['product_id' => '3' , 'order_id' => '3' , 'quantity' => '1', 'price' => '33900000'],
            ['product_id' => '4' , 'order_id' => '4' , 'quantity' => '1', 'price' => '30900000'],
            ['product_id' => '5' , 'order_id' => '5' , 'quantity' => '2', 'price' => '53800000'],
        ];
      foreach ($orderdetails as $orderdetail){
        OrderDetail::factory()
              ->create($orderdetail);
      } 
    }
}
