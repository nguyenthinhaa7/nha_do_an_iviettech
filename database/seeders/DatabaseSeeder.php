<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        \App\Models\User::factory(10)->create();
        // \App\Models\Product::factory(5)->create();
        //  \App\Models\Comment::factory()->create();
        // \App\Models\ProductImage::factory(5)->create();
        // \App\Models\Order::factory(5)->create();
        // \App\Models\OrderDetail::factory(5)->create();
        $this->call(ProductSeeder::class);
        $this->call(ProductImageSeeder::class);
        $this->call(CommentSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(OrderDetailSeeder::class);
    }
}
