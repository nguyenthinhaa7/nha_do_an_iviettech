<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ProfileController;


use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//home
// Route::get('/home', [HomeController::class, 'index'])->name('home.index');
Route::get('/', [HomeController::class, 'index'])->name('home.index');

Route::get('/pay', [CartController::class, 'pay'])->name('cart.pay');

//homedetail

//cart

//search
Route::group(['prefix' => 'product', 'as' => 'product.'], function () {
    Route::get('/', [ProductController::class, 'index'])->name('index');
    
});
//shopping

Route::group(['prefix' => 'product', 'as' => 'product.'], function () {
    Route::get('/detail/{id}', [ProductController::class, 'detail'])->name('detail');
    
});

Route::group(['prefix' => 'cart', 'as' => 'cart.'], function () {
    Route::get('/', [CartController::class, 'index'])->name('index');
    Route::post('/{id}/add', [CartController::class, 'addCart'])->name('add-cart');

    Route::group(['prefix' => 'checkout', 'as' => 'checkout.'], function () {
        // Route for shipping-information
        Route::get('/shipping-information', [CartController::class, 'shippingInformation'])->name('shipping-information');
        Route::post('/handle-shipping-information', [CartController::class, 'handleShippingInformation'])->name('handle-shipping-information');

        // Route for payment-method
        Route::get('/payment-method', [CartController::class, 'paymentMethod'])->name('payment-method');

        // Route for checkout-order
        Route::post('/checkout-order', [CartController::class, 'checkoutOrder'])->name('checkout-order');

        // Route for thank-you
        Route::get('/thank-you', [CartController::class, 'thankYou'])->name('thank-you');
    });
});



Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__.'/auth.php';


Route::group(['prefix' => 'cart', 'as' => 'cart.'], function () {
    Route::get('/', [CartController::class, 'index'])->name('index');
    Route::post('/{id}/add', [CartController::class, 'addCart'])->name('add-cart');
    Route::put('/{id}/update-quantity', [CartController::class, 'updateQuantity'])->name('update-quantity');
    Route::delete('/{id}/remove-product-in-cart', [CartController::class, 'removeProductInCart'])->name('remove-product-in-cart');
});
Route::get('/my-profile', [ProfileController::class, 'myProfile'])->name('my-profile');

Route::post('post/comment/{id}', [ProductController::class, 'store'])
        ->name('post.comment');