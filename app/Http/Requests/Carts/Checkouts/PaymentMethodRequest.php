<?php

namespace App\Http\Requests\Carts\Checkouts;

use Illuminate\Foundation\Http\FormRequest;

class PaymentMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * Chú thích các RULE được sử dụng
         *
         * Rule: required
         * https://laravel.com/docs/8.x/validation#rule-required
         *
         * Rule: integer
         * https://laravel.com/docs/8.x/validation#rule-integer
         */

        return [
            'shipping_method' => 'required|integer',
            'shipping_fee' => 'required|integer',
            'payment_method_id' => 'required|integer',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     *
     * @Custom Attribute
     * https://laravel.com/docs/8.x/validation#specifying-attribute-in-language-files
     */
    public function attributes()
    {
        return [
            'shipping_method' => trans('validation_custom.attributes.shipping_method'),
            'shipping_fee' => trans('validation_custom.attributes.shipping_fee'),
            'payment_method_id' => trans('validation_custom.attributes.payment_method_id'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     *
     * @custom error message
     * https://laravel.com/docs/8.x/validation#customizing-the-error-messages
     */
    public function messages()
    {
        return [
            'shipping_method.required' => trans('validation_custom.messages.shipping_method_required'),
            'shipping_fee.required' => trans('validation_custom.messages.shipping_fee_required'),
        ];
    }
}
