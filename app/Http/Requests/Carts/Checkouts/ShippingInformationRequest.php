<?php

namespace App\Http\Requests\Carts\Checkouts;

use Illuminate\Foundation\Http\FormRequest;

class ShippingInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * Chú thích các RULE được sử dụng
         *
         * Rule: required
         * https://laravel.com/docs/8.x/validation#rule-required
         *
         * Rule: min
         * https://laravel.com/docs/8.x/validation#rule-min
         *
         * Rule: max
         * https://laravel.com/docs/8.x/validation#rule-max
         *
         * Rule: integer
         * https://laravel.com/docs/8.x/validation#rule-integer
         *
         * Rule: email
         * https://laravel.com/docs/8.x/validation#rule-email
         *
         * Rule: nullable
         * https://laravel.com/docs/8.x/validation#rule-nullable
         */

        return [
            'fullname' => 'required|min:2|max:255',
            'phone' => 'required',
            'email' => 'required|email|max:255',
            'address' => 'required|max:255',
            'comment' => 'nullable|max:512',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     *
     * @Custom Attribute
     * https://laravel.com/docs/8.x/validation#specifying-attribute-in-language-files
     */
    public function attributes()
    {
        return [
            'fullname' => trans('validation_custom.attributes.fullname'),
            'phone' => trans('validation_custom.attributes.phone'),
            'email' => trans('validation_custom.attributes.email'),
            'address' => trans('validation_custom.attributes.shipping_address'),
            'comment' => trans('validation_custom.attributes.leave_message'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     *
     * @custom error message
     * https://laravel.com/docs/8.x/validation#customizing-the-error-messages
     */
    public function messages()
    {
        return [
            'fullname.required' => trans('validation_custom.messages.fullname_required'),
            'phone.required' => trans('validation_custom.messages.phone_required'),
        ];
    }
}
