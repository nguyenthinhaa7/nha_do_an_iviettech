<?php

namespace App\Http\Controllers;

use App\Http\Requests\Carts\Checkouts\PaymentMethodRequest;
use App\Http\Requests\Carts\Checkouts\ShippingInformationRequest;
use App\Http\Requests\Carts\CheckQuantityRequest;
use App\Jobs\OrderShippedJob;
use App\Models\Product;
use App\Utils\CartUtil;
use App\Utils\CommonUtil;
use App\Models\OrderDetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $carts = session('carts');
        $data['carts'] = $carts;
        return view('cart.index', $data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function addCart($id)
    // {    
    //     $product = Product::findOrFail($id); 
    //     $carts = session('carts') ?? [];
    //     $newProduct = [
    //         'cart_product_id' => $product->id,
    //         'cart_quantity' => 1,
    //         'cart_product_info' => $product,
    //     ];
    //     array_push($carts, $newProduct);      
    //     session(['carts' => $carts]);
    //     return redirect()->route('cart.index');
    // } 
    public function shippingInformation()
    {
        $data = [];

        // Check if Cart Empty
        if (!session('carts')) {
            return redirect()->route('product.index');
        }

        // Get Info from Session
        $cartShippingInfo = session('cart_shipping_info');
        $data['cartShippingInfo'] = $cartShippingInfo;

        // Get Data in Cart
        $carts = session('carts');

        // Process Data In Cart
        $result = CartUtil::processDataInCart($carts);

        // Set Data
        $data['total_quantity'] = $result['total_quantity'];
        $data['total_amount'] = $result['total_amount'];
        $data['total_amount_discount'] = $result['total_amount_discount'];
        $data['carts'] = $result['carts'];

        return view('cart.checkouts.shipping_information', $data);
    } 
    
    public function addCart($id, Request $request)
    {
        $product = Product::findOrFail($id);  
        $quantity = $product->quantity;
        if ($quantity < 0) {
            return back()
                ->with('error', 'Sản phẩm này đã hết hàng. Vui lòng chọn một sản phẩm khác');
        }
        $carts = session('carts') ?? [];       
        if (!empty($carts)) {
            foreach ($carts as $key => $cart) {
                if ($cart['cart_product_id'] == $product->id) {
                    $quantityUpdate = $cart['cart_quantity'] + 1;
                    $carts[$key]['cart_quantity'] = $quantityUpdate;
                } else {
                    $newProduct = [
                        'cart_product_id' => $product->id,
                        'cart_quantity' => 1,
                        'cart_product_info' => $product,
                    ];
                    array_push($carts, $newProduct);    
                }
            }
        } else {
            $newProduct = [
                'cart_product_id' => $product->id,
                'cart_quantity' => 1,
                'cart_product_info' => $product,
            ];
            array_push($carts, $newProduct);      
        }    
        session(['carts' => $carts]);
        return redirect()->route('cart.index')
                ->with('success', 'Thêm sản phẩm vào giỏ hàng thành công.');
    }

    public function updateQuantity(int $productId, CheckQuantityRequest $request)
    {
        $carts = session('carts');
        if ($carts == null) {
            return redirect()->route('product.index');
        }
        $quantity = $request->quantity;
        $product = Product::findOrFail($productId);
        $quantityDB = $product->quantity;
        if ($quantity > $quantityDB) {
            return redirect()->route('cart.index')->with('error', 'Vượt tồn kho. Vui lòng giảm số lượng.');
        }
        $collectionCarts = collect($carts)->map(function ($cart) use ($productId, $quantity) {
            if ($cart['cart_product_id'] == $productId) {
                $cart['cart_quantity'] = $quantity;
            }
            return $cart;
        });
        $carts = $collectionCarts->toArray();
        session(['carts' => $carts]);
        return redirect()->route('cart.index')
            ->with('success', 'Đã cập nhật quantity cho sản phẩm thành công.');
    }
    public function removeProductInCart($id)
    {
        $product = Product::findOrFail($id);
        $carts = session('carts');
        if (!empty($carts)) {
            foreach ($carts as $key => $value) {
                if ($value['cart_product_id'] == $product->id) {
                    unset($carts[$key]);
                }
            }
        }
        session(['carts' => $carts]);       
        return redirect()->route('cart.index')
            ->with('success', 'Đã xoá sản phẩm ra khỏi giỏ hàng thành công.');
    }

    public function handleShippingInformation(ShippingInformationRequest $request)
    {
        // Collect Info
        $cartShippingInfo = $request->only([
            'fullname',
            'phone',
            'email',
            'address',
            'comment',
        ]);

        // Set $cartShippingInfo to Session
        session(['cart_shipping_info' => $cartShippingInfo]);

        // Return
        return redirect()->route('cart.checkout.payment-method');
    }

    public function paymentMethod()
    {
        $data = [];

        // Check if Cart Empty
        if (!session('carts')) {
            return redirect()->route('product.index');
        }

        // Process Data In Cart
        $result = CartUtil::processDataInCart(session('carts'));

        // Set Data
        $data['total_quantity'] = $result['total_quantity'];
        $data['total_amount'] = $result['total_amount'];
        $data['total_amount_discount'] = $result['total_amount_discount'];
        $data['carts'] = $result['carts'];

        return view('cart.checkouts.payment_method', $data);
    }


    public function checkoutOrder(PaymentMethodRequest $request)
    {
        // Get cart info
        $carts = session('carts');
        // Get Shipping Info from Session
        $data = [];
        $cartShippingInfo = session('cart_shipping_info');
        $data['cartShippingInfo'] = $cartShippingInfo;
        return view('cart.checkouts.thank_you', $data);

        
    }

    /**
     * Thank You function.
     *
     * @return void
     */
    public function thankYou()
    {
        $data = [];

        // Get Data from Session
        $info = session('cart_thank_you');

        // Check Data
        if (empty($info)) {
            return redirect()->route('product.index');
        }
        
        // Get Cart Info
        if (!empty($info['carts'])) {
            $data['carts'] = $info['carts'];
        }

        // Process Data In Cart
        $result = CartUtil::processDataInCart($info['carts']);

        // Set Data
        $data['total_quantity'] = $result['total_quantity'];
        $data['total_amount'] = $result['total_amount'];
        $data['total_amount_discount'] = $result['total_amount_discount'];
        $data['carts'] = $result['carts'];

        // Order Info
        if (!empty($info['order_info'])) {
            $data['orderInfo'] = $info['order_info'];
        }

        // Shipping Info
        if (!empty($info['shipping_info'])) {
            $data['shippingInfo'] = $info['shipping_info'];
        }

        // Payment Method Info
        if (!empty($info['payment_method'])) {
            $data['paymentMethod'] = $info['payment_method'];
        }

        return view('carts.checkouts.thank_you', $data);
    }
    
}

