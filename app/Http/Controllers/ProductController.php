<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Models\Product;
use Exception;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $product;
    public function __construct( Product $product)
    {
        $this->product = $product;
    }
    public function index(Request $request)
    {
        //
        $data = [];
        $products = Product::where('name', 'like', '%' . $request->keyword . '%')->paginate(4);
        //orwheer('content', 'like',...)...
        $data['products'] = $products;
        return view('product.index', $data);
    }
    public function detail(int $id)
    {
        $data = [];
        $product = $this->product->with('productImages')->with('comments')           
            ->findOrFail($id);        
        $data['product'] = $product;
        // display create success
        return view('product.detail', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        //
        Comment::create([
            'user_id' => Auth::id(),
            'product_id'=> $id, 
            'content' =>$request->get('content'),
            'rate' =>$request->get('rate')


        ]);
        return redirect()->back();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
