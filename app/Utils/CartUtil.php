<?php

namespace App\Utils;

use App\Models\Product;
use Illuminate\Support\Facades\Log;

class CartUtil
{
    /**
     * Function Update Quantity.
     *
     * @param array   $carts
     * @param integer $productId
     * @param integer $quantity
     *
     * @return array
     */
    public static function updateQuantity($carts, $productId, $quantity)
    {
        // Convert $carts from array to collection
        $carts = collect($carts)->map(function ($cart) use ($productId, $quantity) {
            if ($cart['cart_product_id'] == $productId) {
                // Update value for attribute quantity
                $cart['cart_quantity'] = $quantity;
            }

            // Return
            return $cart;
        });

        // Convert data from collection to array
        $carts = $carts->toArray();

        // Return
        return $carts;
    }

    /**
     * Function Calculate Summary.
     *
     * @param array $carts
     *
     * @return array
     */
    public static function calcalateSummary($carts)
    {
        // Create Collection
        $collectCart = collect($carts);

        /**
         * Get Product IDs from Cart
         *
         * @reference: https://laravel.com/docs/8.x/collections#method-pluck
         */
        $productIds = $collectCart->pluck('cart_product_id');

        // Get Product List from Product IDs
        $products = Product::whereIn('id', $productIds)
            ->with('singlePromotion')
            ->select([
                'id',
                'category_id',
                'name',
                'thumbnail',
                'price',
            ])
            ->get();


        Log::info(json_encode($collectCart));

        // Calculate for Total Quantity
        $totalQuantity = $collectCart->sum('cart_quantity');

        // Calculate for Total Money
        $totalMoney = $collectCart->sum(function ($cart) {
            return $cart['cart_quantity'] * $cart['product_info']['price'];
        });

        // Return
        return [
            'total_quantity' => $totalQuantity,
            'total_money' => $totalMoney,
        ];
    }

    /**
     * Process Data In Cart function.
     *
     * @param array $carts
     *
     * @return array
     */
    public static function processDataInCart($carts)
    {
        /**
         * Get Product IDs from Cart
         *
         * @reference: https://laravel.com/docs/8.x/collections#method-pluck
         */
        $productIds = collect($carts)->pluck('cart_product_id');

        // Get Product List from Product IDs
        $products = Product::whereIn('id', $productIds)
            ->select([
                'id',
                'name',
                'thumbnail',
                'price',
            ])
            ->get();


        // Calculate Total
        $totalQuantity = 0;
        $totalAmount = 0;
        $totalAmountDiscount = 0;

        foreach ($carts as $key => $cart) {
            /**
             * Get Data of Product
             *
             * @reference: https://laravel.com/docs/8.x/collections#method-first-where
             */
            $productInfo = $products->firstWhere('id', $cart['cart_product_id']);


            

            $priceDiscount = 0;

           


            // Set key price_discount
            $productInfo->price_discount = $priceDiscount;

            // Calculate Amount, Amount Discount
            $amount = $cart['cart_quantity'] * $productInfo->price;
            $amountDiscount = $cart['cart_quantity'] * $priceDiscount;


            // Set Data
            $carts[$key]['amount'] = $amount;
            $carts[$key]['amount_discount'] = $amountDiscount;

            // Calculate Total
            $totalQuantity += $cart['cart_quantity'];
            $totalAmount += $amount;
            $totalAmountDiscount += ($amountDiscount == 0 ? $amount : $amountDiscount);

            // Set $product into $carts
            $carts[$key]['product_info'] = $productInfo;
        }

        // Return Result
        return [
            'total_quantity' => $totalQuantity,
            'total_amount' => $totalAmount,
            'total_amount_discount' => $totalAmountDiscount,
            'carts' => $carts,
        ];
    }
}
