<?php

use Illuminate\Support\Facades\Auth;

if (!function_exists('fe_current_user')) {
    /**
     * Get Current User for Front End.
     *
     * @return Collection
     */
    function fe_current_user()
    {
        return Auth::guard('web')->user();
    }
}

if (!function_exists('be_current_user')) {
    /**
     * Get Current User for Backend End.
     *
     * @return Collection
     */
    function be_current_user()
    {
        return Auth::guard('admin')->user();
    }
}

if (!function_exists('format_price')) {
    /**
     * Format Price.
     *
     * @param double $price
     *
     * @return string
     */
    function format_price($price)
    {
        return number_format($price) . ' đ';
    }
}

if (!function_exists('get_price_discount')) {
    /**
     * Get Price Discount.
     *
     * @param double $price
     * @param array  $promotion
     *
     * @return double
     */
    function get_price_discount($price, $promotion)
    {
        // Type = Percent
        if ($promotion['type'] == 'percent') {
            $priceDiscount = $price * ($promotion['discount'] / 100);

            $newPrice = $price - $priceDiscount;

            return $newPrice;
        }
        

        // Type = Money
        $priceDiscount = $promotion['discount'];

        $newPrice = $price - $priceDiscount;

        return $newPrice;
    }
}

if (!function_exists('display_price_discount')) {
    /**
     * Display Price Discount.
     *
     * @param double $price
     * @param array  $promotion
     *
     * @return string
     */
    function display_price_discount($price, $promotion)
    {
        $newPrice = get_price_discount($price, $promotion);
        
        return number_format($newPrice) . ' đ';
    }
}

if (!function_exists('get_promotion_from_collection')) {
    /**
     * Display Price Discount.
     *
     * @param collect $promotionCollect
     *
     * @return collect
     */
    function get_promotion_from_collection($promotionCollect)
    {
        /**
         * @reference: https://laravel.com/docs/8.x/collections#method-first
         */
        return $promotionCollect->first()
            ? $promotionCollect->first()->toArray() : null;
    }
}
