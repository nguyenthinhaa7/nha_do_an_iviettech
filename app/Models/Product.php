<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'price',
        'content',
        'status',
        'quantity',
        'thumbnail',
    ];
    public function comments(){
        return $this->hasMany(Comment::class);
    }
    
    public function productImages()
    {
        return $this->hasMany(ProductImage::class, 'product_id', 'id');
    }
    
    public function order_details(){
        return $this->hasMany(OrderDetail::class);
    }
    
}
