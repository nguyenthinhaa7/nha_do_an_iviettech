<?php

namespace App\Jobs;

use App\Mail\OrderShipped;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class OrderShippedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $orderCode;
    private $shippingInfo;
    private $cartInfoToSendMail;

    /**
     * Create a new job instance.
     *
     * @param string $orderCode
     * @param mixed  $shippingInfo
     * @param mixed  $cartInfoToSendMail
     *
     * @return void
     */
    public function __construct($orderCode, $shippingInfo, $cartInfoToSendMail)
    {
        $this->orderCode = $orderCode;
        $this->shippingInfo = $shippingInfo;
        $this->cartInfoToSendMail = $cartInfoToSendMail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Mail::to($this->shippingInfo['email'])->send(new OrderShipped(
                $this->orderCode,
                $this->shippingInfo,
                $this->cartInfoToSendMail
            ));

            Log::info('Sent Mail OK.');
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
        }
    }
}
